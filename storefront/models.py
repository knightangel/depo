from statistics import mode
from django.contrib.auth.models import User
from django.db import models
from django.core.files import File

from io import BytesIO
from PIL import Image


class Category(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.title


class Service(models.Model):
    DRAFT = 'draft'
    WAITING_APPROVAL = 'waitingapproval'
    ACTIVE = 'active'
    DELETED = 'deleted'

    STATUS_CHOICES = (
        (DRAFT, 'Draft'),
        (WAITING_APPROVAL, 'Waiting approval'),
        (ACTIVE,'Active'),
        (DELETED, 'Deleted')
    )

    user = models.ForeignKey(User, related_name='services', on_delete=models.CASCADE)
    category = models.ForeignKey(Category, related_name='services', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50)
    description = models.TextField(blank=True)
    location = models.CharField(max_length=50, default='this area')
    image = models.ImageField(upload_to='uploads/service_images', blank=True,null=True)
    thumbnail = models.ImageField(upload_to='uploads/service_images/thumbnail', blank=True,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES, default=ACTIVE)
    phone_number = models.CharField(max_length=20, default='1234567890')
    whatsapp_number = models.CharField(max_length=20, default='1234567890')
    email = models.EmailField(default='example@example.com')

    class Meta:
        ordering = ('-created_at',)

    def __str__(self):
        return self.title

    def get_thumbnail(self):
        if self.thumbnail:
            return self.thumbnail.url
        else:
            if self.image:
                self.thumbnail = self.make_thumbnail(self.image)
                self.save()

                return self.thumbnail.url
            else:
                return 'https://via.placeholder.com/240x240x.jpg'

    def make_thumbnail(self, image, size=(300, 300),  format='JPEG'):
        img = Image.open(image)

        img = img.convert('RGB')

        img.thumbnail(size)

        thumb_io = BytesIO()
        img.save(thumb_io, format, quality=85)
        name = image.name.replace('uploads/service_images/', '')
        thumbnail = File(thumb_io, name=name)

        return thumbnail


class Review(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rating = models.IntegerField()  # You can choose the type of rating field you need
    comment = models.TextField()